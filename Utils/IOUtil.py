import os
import pickle
import sys
import zipfile

import numpy as np
import pandas as pandas

data_root = '/home/pietro/Documents/Coding/Datasets/Vibration'


def maybe_extract(filename, folder_name, force=False):
    relative_folder_name = data_root + folder_name
    root = os.path.splitext(os.path.splitext(filename)[0])[0]  # remove .tar.gz
    if os.path.isdir(relative_folder_name) and not force:
        # You may override by setting force=True.
        print('%s already present - Skipping extraction of %s.' % (root, filename))
    else:
        print('Extracting data for %s. This may take a while. Please wait.' % root)
        archive = zipfile.ZipFile(filename, 'r')
        sys.stdout.flush()
        archive.extractall(data_root)
        archive.close()
    data_folders = [
        os.path.join(relative_folder_name, d) for d in sorted(os.listdir(relative_folder_name))
        if os.path.isdir(os.path.join(relative_folder_name, d))]
    print(data_folders)
    return data_folders


def maybe_pickle(data_folder, force=False):
    dataset_names = []
    for folder in data_folder:
        set_filename = folder + '.pickle'
        dataset_names.append(set_filename)
        if os.path.exists(set_filename) and not force:
            # You may override by setting force=True.
            print('%s already present - Skipping pickling.' % set_filename)
        else:
            print('Pickling %s.' % set_filename)
            dataset = load_bearing(folder)
            try:
                with open(set_filename, 'wb') as f:
                    pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
            except Exception as e:
                print('Unable to save data to', set_filename, ':', e)
    return dataset_names


def load_bearing(folder):
    bearing_files = os.listdir(folder)
    bearing_dict = {}
    num_bearings = 0
    for bearing in bearing_files:
        bearing_file = os.path.join(folder, bearing)
        try:
            filename = (os.path.splitext(bearing_file)[0]).split('/')[-1]
            # print(filename)
            if str(filename).split('_')[0] == "temp":
                continue
            bearing_data = pandas.read_csv(bearing_file, header=None, delimiter=',',
                                           names=['hour', 'minute', 'second', 'usecond', 'hacc', 'vacc'])
            # print(bearing_data.head())
            bearing_dict[filename] = bearing_data
            num_bearings += 1
        except IOError as e:
            print('Could not read:', bearing_file, ':', e, '- it\'s ok, skipping.')

    dataset = pandas.concat(bearing_dict)
    print(dataset)
    print('Full dataset tensor:', dataset.shape)
    print('Mean:', np.mean(dataset))
    print('Standard deviation:', np.std(dataset))
    return dataset


def load_pickle(pickle_files):
    train_data = {}
    for label, pickle_file in enumerate(pickle_files):
        try:
            with open(pickle_file, 'rb') as f:
                # print(pickle_file, label)
                measurement_name = str(pickle_file).split("/")[-1].split(".")[0]
                # print(measurement_name)
                bearing_set = pickle.load(f)
                train_data[measurement_name] = bearing_set
        except Exception as e:
            print('Unable to process data from', pickle_file, ':', e)
            raise
        # TODO: remove me
        break
    return train_data
