import matplotlib.pyplot as plt
import numpy as np
from sympy.external.tests.test_scipy import scipy


def plot_analysis_dataset(datasets, datasets_fq):
    for name, bearing in datasets.items():
        print('Plotting', name)
        y = ['hacc', 'hvel', 'vacc', 'vvel']
        for y_name in y:
            plt.figure()
            # plot input data against time
            plt.subplot(2, 1, 1)
            plt.plot(bearing.index, bearing[y_name], label='input data')
            plt.xlabel('time [s]')
            plt.ylabel('signal')

            # plot frequency spectrum
            plt.subplot(2, 1, 2)
            plt.plot(datasets_fq[name]['f'], datasets_fq[name]['a_' + y_name],
                     label='abs(fourier transform)')
            plt.xlabel('frequency [Hz]')
            plt.ylabel('abs(DFT(signal))')
            plt.draw()
        plt.show()


def plot_time_domain_waveform(datasets, y='hacc'):
    for name, bearing in datasets.items():
        print('Plotting', name)
        # print(bearing.head(2000))

        # compute fourier transform
        f = scipy.fft(bearing[y])

        print(bearing.shape)
        # work out meaningful frequencies in fourier transform
        freqs = scipy.arange(0, (bearing.shape[0] - 1) / 2., dtype='d') / 0.00390625  # d=double precision float
        n_freq = len(freqs)

        # plot input data y against time
        plt.subplot(2, 1, 1)
        plt.plot(bearing.index, bearing[y], label='input data')
        plt.xlabel('time [s]')
        plt.ylabel('signal')

        # plot frequency spectrum
        plt.subplot(2, 1, 2)
        plt.plot(freqs, abs(f[0:n_freq]),
                 label='abs(fourier transform)')
        plt.xlabel('frequency [Hz]')
        plt.ylabel('abs(DFT(signal))')

        bearing.plot(y='hacc')
        plt.show()
        plt.close()


def plot_example_segment_fit(clusterer, windowed_segment):
    centroids = clusterer.cluster_centers_

    # remember, the clustering was set up using the windowed data
    # so to find a match, we should also window our search key
    test_windowed_segment = np.copy(windowed_segment)
    nearest_centroid_idx = clusterer.predict(test_windowed_segment.reshape(1, -1))[0]
    nearest_centroid = np.copy(centroids[nearest_centroid_idx])
    plt.figure()
    # plt.plot(segment, label="Original segment")
    print(test_windowed_segment)
    print(nearest_centroid)
    plt.plot(test_windowed_segment, label="Windowed segment")
    plt.plot(nearest_centroid, label="Nearest centroid")
    plt.legend()
    plt.show()
    plt.close()


def plot_waves(waves, step):
    """
    Plot a set of 9 waves from the given set, starting from the first one
    and increasing in index by 'step' for each subsequent graph
    """
    plt.figure()
    n_graph_rows = 3
    n_graph_cols = 3
    graph_n = 1
    wave_n = 0
    for _ in range(n_graph_rows):
        for _ in range(n_graph_cols):
            axes = plt.subplot(n_graph_rows, n_graph_cols, graph_n)
            plt.plot(waves[wave_n])
            graph_n += 1
            wave_n += step
    # fix subplot sizes so that everything fits
    plt.tight_layout()
    plt.show()


def plot_reconstruction(df, reconstruction, error):
    plt.figure()
    plt.plot(df['f'], df['a_vacc'], label="Original")
    plt.plot(df['f'], reconstruction, label="Reconstructed")
    plt.plot(df['f'], error, label="Reconstruction error")
    plt.legend()
    plt.show()
