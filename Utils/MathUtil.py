from audioop import avg

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy
from scipy import integrate, signal
from sklearn.cluster import KMeans

from Utils.PlotUtil import plot_example_segment_fit, plot_reconstruction

freq_domain = ['a_vacc', 'a_hacc', 'a_vvel', 'a_hvel']


def calculate_velocity(datasets):
    for name, bearing in datasets.items():
        # print(integrate.cumtrapz(list(bearing['hacc']), initial=0))
        bearing['hvel'] = integrate.cumtrapz(bearing['hacc'], bearing.index, initial=0)
        bearing['vvel'] = integrate.cumtrapz(bearing['vacc'], bearing.index, initial=0)
        # print(bearing.shape)


def frequency_analysis(u, t):
    window = signal.hann(t)
    A = scipy.fft(u * window)
    sample_rate = 256  # sampling rate
    dt = 1. / sample_rate
    freq = np.arange(t / 2, dtype=float) / t / dt
    A = abs(A[0:freq.size]) / t
    return freq[:- 1], A[:- 1]


def calculate_freq_domain(datasets):
    fq_datasets = {}
    for name, df in datasets.items():
        f, a = frequency_analysis(df['hacc'], len(df.index))
        freq_df = pd.DataFrame({'f': f, 'a_hacc': a})
        _, a = frequency_analysis(df['vacc'], len(df.index))
        freq_df['a_vacc'] = a
        _, a = frequency_analysis(df['hvel'], len(df.index))
        freq_df['a_hvel'] = a
        _, a = frequency_analysis(df['vvel'], len(df.index))
        freq_df['a_vvel'] = a
        fq_datasets[name] = freq_df
        print("F datapoints:", len(f))
    return fq_datasets


def refactor_data(datasets):
    for name, bearing in datasets.items():
        bearing.sort_values(by=['hour', 'minute', 'second', 'usecond'], inplace=True)
        bearing.reset_index(drop=True, inplace=True)


def resampling_freq_domain(training_data_fq, freq_datapoint_number=10000):
    resampled_data_dic = {}
    for name, bearing in training_data_fq.items():
        tick = int(float(bearing.shape[0]) / freq_datapoint_number)
        s = (bearing.index.to_series() / tick).astype(int)

        resampled_data = bearing.groupby(s).mean().set_index(s.groupby(s).apply(lambda x: np.mean(x.index)))
        resampled_data_dic[name] = resampled_data
    return resampled_data_dic


def apply_windowing(training_data_fq, segment_len=32, slide_len=2):
    segment_map = {}

    for name, bearing in training_data_fq.items():
        segments = []
        for start_pos in range(0, bearing.shape[0], slide_len):
            end_pos = start_pos + segment_len
            # make a copy so changes to 'segments' doesn't modify the original ekg_data
            segment = np.copy(bearing['a_vacc'][start_pos:end_pos])
            # if we're at the end and we've got a truncated segment, drop it
            if len(segment) != segment_len:
                continue
            segments.append(segment)

        window_rads = np.linspace(0, np.pi, segment_len)
        window = np.sin(window_rads) ** 2

        windowed_segments = []
        for segment in segments:
            windowed_segment = np.copy(segment) * window
            windowed_segments.append(windowed_segment)

        segment_map[name] = windowed_segments
        print("Produced %d waveform segments" % len(windowed_segments))
    return segment_map


def agglomerate_segments(segment_map):
    agg_segment = {}
    for fq_name in freq_domain:
        agg_segment[fq_name] = []

    for name, mesurement_map in segment_map.items():
        for fq_name in freq_domain:
            agg_segment[fq_name] += mesurement_map[fq_name]

    return agg_segment


def sliding_chunker(data, window_len, slide_len):
    """
    Split a list into a series of sub-lists, each sub-list window_len long,
    sliding along by slide_len each time. If the list doesn't have enough
    elements for the final sub-list to be window_len long, the remaining data
    will be dropped.
    """
    chunks = []
    for pos in range(0, len(data), slide_len):
        chunk = np.copy(data[pos:pos + window_len])
        if len(chunk) != window_len:
            continue
        chunks.append(chunk)

    return chunks


def plot_waves(waves, step):
    """
    Plot a set of 9 waves from the given set, starting from the first one
    and increasing in index by 'step' for each subsequent graph
    """
    plt.figure()
    n_graph_rows = 3
    n_graph_cols = 3
    graph_n = 1
    wave_n = 0
    for _ in range(n_graph_rows):
        for _ in range(n_graph_cols):
            axes = plt.subplot(n_graph_rows, n_graph_cols, graph_n)
            # print(waves[wave_n])
            plt.plot(waves[wave_n])
            graph_n += 1
            wave_n += step
    # fix subplot sizes so that everything fits
    plt.tight_layout()
    plt.show()


def reconstruct(data, window, clusterer):
    """
    Reconstruct the given data using the cluster centers from the given clusterer.
    """
    window_len = len(window)
    slide_len = int(window_len / 2)
    segments = sliding_chunker(data, window_len, slide_len)
    reconstructed_data = np.zeros(len(data))
    for segment_n, segment in enumerate(segments):
        # window the segment so that we can find it in our clusters which were
        # formed from windowed data
        segment *= window
        nearest_match_idx = clusterer.predict(segment.reshape(1, -1))[0]
        nearest_match = np.copy(clusterer.cluster_centers_[nearest_match_idx])

        pos = segment_n * slide_len
        reconstructed_data[pos:pos + window_len] += nearest_match

    return reconstructed_data


def get_windowed_segments(datasets, window):
    """
    Populate a list of all segments seen in the input data.  Apply a window to
    each segment so that they can be added together even if slightly
    overlapping, enabling later reconstruction.
    """

    windows_segment_map = {}
    step = 2
    for name, df in datasets.items():
        fq_segments = {}
        for fq_name in freq_domain:
            windowed_segments = []
            segments = sliding_chunker(list(df[fq_name]), window_len=len(window), slide_len=step)
            for segment in segments:
                segment *= window
                windowed_segments.append(segment)
            fq_segments[fq_name] = windowed_segments
            print("Produced", len(windowed_segments), "waveform segments")
        windows_segment_map[name] = fq_segments
    return windows_segment_map


def get_windowed_segments_test(data, window):
    step = 2
    windowed_segments = []
    segments = sliding_chunker(list(data), window_len=len(window), slide_len=step)
    for segment in segments:
        segment *= window
        windowed_segments.append(segment)
    print("Produced", len(windowed_segments), "waveform segments")
    return windowed_segments


def generate_time_slice_data(dataset_map):
    time_sliced_testing = {}
    slices = [25, 50, 75, 100]
    for name, df in dataset_map.items():
        for slice in slices:
            time_sliced_testing[name + '_' + str(slice)] = df[df.index < len(df.index) * slice / 100].copy()
    return time_sliced_testing


def generate_baseline_data(datasets):
    baseline_map = {}
    for name, df in datasets.items():
        baseline_map[name] = df.loc[df['hour'] == df['hour'].min()].copy()
        # print(baseline_map[name].head())
        # print(baseline_map[name].tail())
    return baseline_map


def cluster_fq_segments(windowed_baseline, n_cluster=150):
    clusterers = {}
    for fq_name in freq_domain:
        clusterers[fq_name] = KMeans(n_clusters=n_cluster)
        clusterers[fq_name].fit(windowed_baseline[fq_name])
        print("Fit Cluster for", fq_name)
        print("Plotting waves")
        plot_waves(clusterers[fq_name].cluster_centers_, step=2)
        # print(windowed_baseline[fq_name][32])
        plot_example_segment_fit(clusterers[fq_name], windowed_baseline[fq_name][32])
    return clusterers


def cluster_training_segments(windowed_baseline, windowed_training, window, sampled_wave_baseline,
                              sampled_wave_training, n_cluster=150):
    agg_min_value = {}
    agg_max_value = {}

    for fq_name in freq_domain:
        agg_min_value[fq_name] = []
        agg_max_value[fq_name] = []

    for name, mesurement_map in windowed_baseline.items():
        for fq_name in freq_domain:
            clusterer = KMeans(n_clusters=n_cluster)
            clusterer.fit(mesurement_map[fq_name])
            plot_waves(clusterer.cluster_centers_, step=2)
            plot_example_segment_fit(clusterer, mesurement_map[fq_name][32])
            score = calculate_score_wave(clusterer, sampled_wave_baseline[name][fq_name], window)
            agg_min_value[fq_name].append(score)

    for name, mesurement_map in windowed_training.items():
        for fq_name in freq_domain:
            clusterer = KMeans(n_clusters=n_cluster)
            clusterer.fit(mesurement_map[fq_name])
            score = calculate_score_wave(clusterer, sampled_wave_training[name][fq_name], window)
            agg_max_value[fq_name].append(score)

    for fq_name in freq_domain:
        agg_min_value[fq_name] = sum(agg_min_value[fq_name]) / len(agg_min_value[fq_name])
        agg_max_value[fq_name] = sum(agg_max_value[fq_name]) / len(agg_max_value[fq_name])

    return agg_min_value, agg_max_value


def calculate_score_wave(clusterer, wave, window):
    reconstruction = reconstruct(list(wave), window, clusterer)
    error = abs(reconstruction - list(wave))
    error_98th_percentile = np.percentile(error, 98)
    print("Maximum reconstruction error is", max(error))
    print("98th percentile of reconstruction error was", error_98th_percentile)
    print("Total error", sum(error))
    # plot_reconstruction(df, reconstruction, error)
    return max(error)


def reconstruct_wave(health_score, datasets, window, n_cluster=150):
    for name, df in datasets.items():
        print("-------Reconstructing for:", name)
        for fq_name in freq_domain:
            print("Reconstructing", fq_name)
            windowed_segments = get_windowed_segments_test(df[fq_name], window)
            clusterer = KMeans(n_clusters=n_cluster)
            clusterer.fit(windowed_segments)
            reconstruction = reconstruct(list(df[fq_name]), window, clusterer)
            error = abs(reconstruction - list(df[fq_name]))
            error_98th_percentile = np.percentile(error, 98)
            print("Maximum reconstruction error is", max(error))
            print("Heal Score of", (max(error) / health_score[1][fq_name]) * health_score[0][fq_name] * 100)
            # print("98th percentile of reconstruction error was", error_98th_percentile)
            # print("Total error", sum(error))
            plot_reconstruction(df, reconstruction, error)
