import numpy as np

from Utils.IOUtil import maybe_extract, maybe_pickle, load_pickle
from Utils.MathUtil import calculate_velocity, refactor_data, calculate_freq_domain, agglomerate_segments, \
    get_windowed_segments, resampling_freq_domain, generate_baseline_data, \
    cluster_fq_segments, reconstruct_wave, generate_time_slice_data, cluster_training_segments
from Utils.PlotUtil import plot_analysis_dataset

WINDOW_LEN = 16
train_filename = '/home/pietro/Documents/Coding/Datasets/Training_set.zip'
test_filename = '/home/pietro/Documents/Coding/Datasets/Full_Test_Set.zip'

if __name__ == "__main__":
    print("Loading Data...")
    train_folders = maybe_extract(train_filename, "/Learning_set")
    test_folders = maybe_extract(test_filename, "/Full_Test_Set")

    train_datasets = maybe_pickle(train_folders)
    test_datasets = maybe_pickle(test_folders)

    training_data = load_pickle(train_datasets)
    testing_data = load_pickle(test_datasets)
    print("Refactoring Data...")
    refactor_data(training_data)
    refactor_data(testing_data)
    testing_data = generate_time_slice_data(testing_data)
    baseline_data = generate_baseline_data(training_data)

    print("Calculating Velocity...")
    calculate_velocity(testing_data)
    calculate_velocity(baseline_data)
    calculate_velocity(training_data)

    print("Calculating Frequency Domain for Acceleration and Velocity...")
    testing_data_fq = calculate_freq_domain(testing_data)
    baseline_fq = calculate_freq_domain(baseline_data)
    training_fq = calculate_freq_domain(training_data)

    print("Re-sampling Frequency Domain Data...")
    resampled_testing_data = resampling_freq_domain(testing_data_fq, 20000)
    resampled_baseline_fq = resampling_freq_domain(baseline_fq, 20000)
    resampled_training_fq = resampling_freq_domain(training_fq, 20000)
    plot_analysis_dataset(baseline_data, resampled_baseline_fq)

    window_rads = np.linspace(0, np.pi, WINDOW_LEN)
    window = np.sin(window_rads) ** 2
    print("Windowing data")
    windowed_baseline = get_windowed_segments(resampled_baseline_fq, window)
    windowed_training = get_windowed_segments(resampled_training_fq, window)

    # agg_windowed_baseline = agglomerate_segments(windowed_baseline)
    # agg_windowed_training = agglomerate_segments(windowed_training)

    print("Clustering")
    health_score = cluster_training_segments(windowed_baseline, windowed_training, window, resampled_baseline_fq,
                                             resampled_training_fq)

    # clusterers = cluster_fq_segments(agg_windowed_baseline)
    # clusterers_training = cluster_fq_segments(agg_windowed_training)

    # print("Reconstructing baselines")
    # reconstruct_wave(clusterers, resampled_baseline_fq, window)

    # print("Reconstructing training")
    # reconstruct_wave(clusterers_training, resampled_training_fq, window)

    print("Reconstructing test data")
    reconstruct_wave(health_score, resampled_testing_data, window)
